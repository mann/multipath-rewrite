package edu.missouri.multipath;

import static org.junit.Assert.assertEquals;
import static edu.missouri.multipath.helpers.PhysicalPathHelper.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MultipathTopologyStepdefs {
    
    Topology topology;
    int counter = 0;
    Map<PhysicalPath, Connection> connections = 
            new HashMap<PhysicalPath, Connection>();
    List<VLRequest> requests = new ArrayList<VLRequest>();
    
    
    @Given("^I have a topology$")
    public void given_I_have_a_topology() {
        topology = new Topology(null);
    }

    @Given("^I add a virtual path with (\\d+)Mbps to the topology$")
    public void and_I_add_a_virtual_path_with_Mbps_to_the_topology(int bandwidth) {
        PhysicalPath path = createPathFromString("path_" + ++counter);
        path.setBandwidth(bandwidth);
        
        topology.addPath(path);
    }
    
    @Given("^I add another virtual path with (\\d+)Mbps to the topology$")
    public void and_I_add_another_virtual_path_with_Mbps_to_the_topology(int bandwidth) {
        and_I_add_a_virtual_path_with_Mbps_to_the_topology(bandwidth);
    }
    
    @When("^I make (\\d+) mutlipath requests$")
    public void when_I_make_n_multipath_requests(int n) {
        requests.clear();
        for (int i=0; i<n; i++) {
            VLRequest request = new VLRequest("h1", 22, "h2", 23);
            Connection connection = topology.multiPath(request);
            
            connections.put(connection.getPath(), connection); 
            requests.add(request);
        }
    }
    
    @Then("^request (\\d+) should get (\\d+)Mbps$")
    public void then_request_j_should_get_k_Mbps(int j, int k) {
        for (Connection conn : connections.values()) {
            if (conn.contains(requests.get(j-1))) {
                assertEquals(k, conn.getBandwidth(), 0);
            }
        }
    }
}
