package edu.missouri.multipath;

import static org.junit.Assert.assertEquals;
import static edu.missouri.multipath.helpers.PhysicalPathHelper.*;

import java.util.Arrays;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PhysicalPathStepdefs {

    private PhysicalPath path;
    private String pathStr;
    private double bandwidth;
    private PhysicalPath[] paths;
    
    @Given("^I have a virtual link path with route '(.*)'$")
    public void given_I_have_a_virtual_link_path_with_route(String route)
    {
        path = createPathFromString(route);
    }
    
    @When("^I call path.toString$")
    public void when_I_call_path_toString()
    {
        pathStr = path.toString();
    }
    
    @Then("^the path string should be '(.*)'$")
    public void then_the_path_string_should_be(String expected) {
        assertEquals(expected, pathStr);
    }
    
    @Given("^I have a virtual link path$")
    public void given_I_have_a_virtual_link_path()
    {
        path = createPathFromString("a->virtual->link->path");
    }
    
    @And("^I set bandwidth to (\\d+)Mbps$")
    public void and_I_set_bandwidth_to(double bandwidth) {
        path.setBandwidth(bandwidth);
    }
    
    @When("^I get bandwidth$")
    public void when_I_get_bandwidth() {
        bandwidth = path.getBandwidth();
    }
    
    @Then("^the bandwidth should be (\\d+)Mbps$")
    public void then_the_bandwidth_should_be(double expected) {
        assertEquals(expected, bandwidth, 0);
    }
    
    @Given("^I have (\\d+) virtual link paths$")
    public void i_have_k_virtual_link_paths(int k) {
        paths = new PhysicalPath[k];
        for (int i=0; i<k; i++) {
            paths[i] = createPathFromString("path_" + i+1);
        }
    }

    @Given("^path (\\d+) has (\\d+)Mbps bandwidth$")
    public void and_path_j_has_k_Mbps_bandwidth(int j, int k) {
        paths[j-1].setBandwidth(k);
    }

    @When("^I sort the virtual link paths$")
    public void i_sort_the_virtual_link_paths() {
        Arrays.sort(paths);
    }

    @Then("^the (\\d+)th path should has (\\d+)Mbps$")
    public void the_j_path_should_has_k_Mbps(int j, double k) {
        assertEquals(k, paths[j-1].getBandwidth(), 0); 
    }
    
    @Then("^the (\\d+)st path should has (\\d+)Mbps$")
    public void the_j_1st_path_should_has_k_Mbps(int j, double k) {
        the_j_path_should_has_k_Mbps(j, k);
    }
    
    @Then("^the (\\d+)nd path should has (\\d+)Mbps$")
    public void the_j_2nd_path_should_has_k_Mbps(int j, double k) {
        the_j_path_should_has_k_Mbps(j, k);
    }
    
    @Then("^the (\\d+)rd path should has (\\d+)Mbps$")
    public void the_j_3rd_path_should_has_k_Mbps(int j, double k) {
        the_j_path_should_has_k_Mbps(j, k);
    }
}
