package edu.missouri.multipath;

import static org.junit.Assert.assertEquals;
import static edu.missouri.multipath.helpers.PhysicalPathHelper.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ConnectionStepdefs {
    
    Connection connection;
    double bandwidth;
    double cost;
    
    @Given("^A virtual connection has (\\d+)Mbps bandwidth path$")
    public void a_virtual_connection_has_k_Mbps_bandwidth_path(int k) {
        PhysicalPath path = createPathFromString("path_1");
        path.setBandwidth(10);
        connection = new Connection(path);
    }

    @Given("^contains (\\d+) requests$")
    public void contains_k_requests(int k) {
        for (int i=0; i<k; i++) {
            VLRequest request = new VLRequest("h1", 22, "h2", 23);
            connection.addRequest(request);
        }
    }

    @When("^I query for bandwidth$")
    public void i_query_for_bandwidth() {
        bandwidth = connection.getBandwidth();
    }
    
    @When("^I query for cost$")
    public void i_query_for_cost() throws Throwable {
        cost = connection.getCost();
    }

    @Then("^the result cost should be (\\d+)Mbps$")
    public void the_result_cost_should_be_k_Mbps(int k) {
        assertEquals(k, cost, 0);
    }

    @Then("^the result bandwidth should be (\\d+)Mbps$")
    public void the_result_bandwidth_should_be_k_Mbps(int k) {
        assertEquals(k, bandwidth, 0);
    }
}
