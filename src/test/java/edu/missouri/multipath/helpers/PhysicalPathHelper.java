package edu.missouri.multipath.helpers;

import java.util.Arrays;

import edu.missouri.multipath.PhysicalPath;

public class PhysicalPathHelper {

    public static PhysicalPath createPathFromString(String strRoute) {
        return new PhysicalPath( Arrays.asList(strRoute.split("->")) );
    }
}
