Feature: Virtual link path

  Scenario: Convert virtual link path to string
    Given I have a virtual link path with route 'h1->s1->s2->h1'
    When I call path.toString
    Then the path string should be '[h1, s1, s2, h1]'

  Scenario: Assign available bandwidth
    Given I have a virtual link path
    And I set bandwidth to 10Mbps
    When I get bandwidth
    Then the bandwidth should be 10Mbps
    
  Scenario: Natural ordering
    Given I have 3 virtual link paths
    And path 1 has 10Mbps bandwidth
    And path 2 has 20Mbps bandwidth
    And path 3 has 8Mbps bandwidth
    When I sort the virtual link paths
    Then the 1st path should has 20Mbps
    And the 2nd path should has 10Mbps
    And the 3rd path should has 8Mbps