Feature: Multipath topology

  Scenario: Route one request
    Given I have a topology
    And I add a virtual path with 10Mbps to the topology
    When I make 1 mutlipath requests
    Then request 1 should get 10Mbps
    
  Scenario: Route two requests
    Given I have a topology
    And I add a virtual path with 10Mbps to the topology
    And I add another virtual path with 8Mbps to the topology
    When I make 2 mutlipath requests
    Then request 1 should get 10Mbps
    And request 2 should get 8Mbps
    
  Scenario: Route three requests
    Given I have a topology
    And I add a virtual path with 10Mbps to the topology
    And I add another virtual path with 8Mbps to the topology
    When I make 3 mutlipath requests
    Then request 1 should get 5Mbps
    And request 2 should get 8Mbps
    And request 3 should get 5Mbps

  Scenario: Route four requests
    Given I have a topology
    And I add a virtual path with 10Mbps to the topology
    And I add another virtual path with 8Mbps to the topology
    When I make 4 mutlipath requests
    Then request 1 should get 5Mbps
    And request 2 should get 4Mbps
    And request 3 should get 5Mbps
    And request 4 should get 4Mbps