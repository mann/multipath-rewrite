Feature: Virtual Connection

  Scenario: Get bandwidth
    Given A virtual connection has 10Mbps bandwidth path
    And contains 5 requests
    When I query for bandwidth
    Then the result bandwidth should be 2Mbps

  Scenario: Get cost
    Given A virtual connection has 10Mbps bandwidth path
    And contains 2 requests
    When I query for cost
    Then the result cost should be 10Mbps