package edu.missouri.multipath;

import java.util.List;

public class PhysicalPath implements Comparable<PhysicalPath> {

    private List<String> route;
    private double bandwidth;
    private double priority;
    
    public PhysicalPath(List<String> route) {
        this.route = route;
    }
    
    public List<String> getRoute() {
        return route;
    }

    @Override
    public String toString() {
        return route.toString();
    }

    public void setBandwidth(double bandwidth) {
        this.bandwidth = bandwidth;
        setPriority(bandwidth);
    }

    public double getBandwidth() {
        return bandwidth;
    }
    
    public void setPriority(double priority) {
        this.priority = priority;
    }
    
    public double getPriority() {
        return priority;
    }

    public int compareTo(PhysicalPath other) {
        if (priority < other.priority) return 1;
        else if (priority == other.priority) return 0;
        else 
            return -1;
    }
}
