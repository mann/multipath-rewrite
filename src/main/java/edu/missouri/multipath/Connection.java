package edu.missouri.multipath;

import java.util.HashSet;
import java.util.Set;

public class Connection {

    private PhysicalPath path;
    
    private Set<VLRequest> requests = new HashSet<VLRequest>();
    private VLRequest lastRequest = null;
    
    public Connection(PhysicalPath path) {
        this.path = path;
    }

    public double getBandwidth() {
        return path.getBandwidth() / getTotalRequests();
    }
    
    public double getCost() {
        return path.getBandwidth();
    }
    
    public void addRequest(VLRequest request) {
        requests.add(request);
        lastRequest = request;
    }

    public PhysicalPath getPath() {
        return path;
    }

    public int getTotalRequests() {
        return requests.size();
    }

    public boolean contains(VLRequest request) {
        return requests.contains(request);
    }
    
    public Set<VLRequest> getRequests() {
        return requests;
    }
    
    public VLRequest getLastRequest() {
        return lastRequest;
    }
    
    @Override
    public int hashCode() {
        int hash = 17;
        hash = 31 * hash + path.toString().hashCode();
        
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        Connection conn = (Connection) obj;
        return path.toString() == conn.path.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        String pathStr = path.toString(); 
    
        sb.append(lastRequest.toString())
            .append(" [ "+ getCost() +"Mbps]: ")
            .append(pathStr)
            .append("\n");
        return sb.toString();
    }
}
