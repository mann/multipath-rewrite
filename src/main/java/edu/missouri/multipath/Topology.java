package edu.missouri.multipath;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class Topology {

    /**
     * All known virtual link paths
     */
    private Queue<PhysicalPath> paths = new PriorityQueue<PhysicalPath>();
    
    /**
     * connections
     */
    private Map<PhysicalPath, Connection> connections = 
            new HashMap<PhysicalPath, Connection>();
    
    public Topology(Set<PhysicalPath> paths) {
        if (paths != null) {
            for (PhysicalPath path : paths) {
                addPath(path);
            }
        }
    }
    
    /**
     * Add path to the topology to be used in multipath routing
     * 
     * @param path
     */
    public void addPath(PhysicalPath path) {
        paths.add(path);
        connections.put(path, new Connection(path));
    }

    /**
     * Multipath algorithm - distribute and balance all previous and current
     * request to maximize the bandwidth of each available path in the topology.
     * 
     * <p>
     * eg. The distribution of 5 requests in the topology of 4 paths with
     * different bandwidth capacities:<br/>
     * <br/>
     * 
     * path1 (10Mbps): 2 requests [5Mbps each]<br/>
     * path2 (7Mbps): 2 requests [3Mbps each]<br/>
     * path3 (6Mbps): 1 request [6Mbps each]<br/>
     * path4 (1Mbps): 0 request<br/>
     * </p>
     * 
     * @param request
     *            - user request for virtual link
     * @return A list of connections
     */
    public Connection multiPath(VLRequest request) {
        //TODO find path corresponding to the request 
        PhysicalPath path = paths.poll();
        Connection connection = connections.get(path);
        
        //TODO add description of priority
        double priority = path.getBandwidth() / (connection.getTotalRequests() + 2);
        path.setPriority(priority);
        
        connection.addRequest(request);
        paths.add(path);

        return connection;
    }
}
