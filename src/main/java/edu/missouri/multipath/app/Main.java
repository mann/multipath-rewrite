package edu.missouri.multipath.app;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import edu.missouri.multipath.Connection;
import edu.missouri.multipath.Topology;
import edu.missouri.multipath.VLRequest;
import edu.missouri.multipath.PhysicalPath;

public class Main {

    public static void main(String[] args) {
        // Setup two virtual paths
        Set<PhysicalPath> paths = new HashSet<PhysicalPath>();
        paths.add(createPath("h1->s1->s2->s4->h2", 10));
        paths.add(createPath("h1->s1->s3->s4->h2", 10));
        
        // Create a topology with two virtual paths
        Topology topology = new Topology(paths);
        
        // Simulate two multipath requests
        VLRequest[] requests = new VLRequest[] {
                new VLRequest("h1", 5161, "h2", 22),
                new VLRequest("h1", 5162, "h2", 22),
                new VLRequest("h1", 5163, "h2", 22),
                new VLRequest("h1", 5164, "h2", 22),
                new VLRequest("h1", 5165, "h2", 22),
                new VLRequest("h1", 5166, "h2", 22),
                new VLRequest("h1", 5167, "h2", 22),
                new VLRequest("h1", 5168, "h2", 22),
                new VLRequest("h1", 5169, "h2", 22),
                new VLRequest("h1", 5170, "h2", 22)
        };
        
        // Print result
        StringBuilder sb = new StringBuilder();
        
        for (int i=0; i<requests.length; i++) {
            sb.append("Request: ").append(i+1)
                .append("\n");
            
            Connection connection = topology.multiPath(requests[i]);
            sb.append(connection).append("\n");
        }
        System.out.println(sb.toString());
    }

    private static PhysicalPath createPath(String route, int bandwidth) {
        PhysicalPath path = new PhysicalPath(
                Arrays.asList( route.split("->") ));
        path.setBandwidth(bandwidth);
        return path;
    }
}
