package edu.missouri.multipath;

public class VLRequest {

    private String srcIP;
    private int    srcPort;
    private String dstIP;
    private int    dstPort;

    public VLRequest(String srcIP, int srcPort, String dstIP, int dstPort) {
        this.srcIP = srcIP;
        this.srcPort = srcPort;
        this.dstIP = dstIP;
        this.dstPort = dstPort;
    }
    
    public String getSrcIP() {
        return srcIP;
    }
    
    public String getDstIP() {
        return dstIP;
    }
    
    public int getSrcPort() {
        return srcPort;
    }
    
    public int getDstPort() {
        return dstPort;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(srcIP).append(":").append(srcPort)
            .append(" -> ")
            .append(dstIP).append(":").append(dstPort);
        
        return sb.toString();
    }
}
